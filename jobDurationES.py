#coding:utf-8
__author__ = 'akosyrev'

import os
import urllib2
import json
import sqlite3
import math
from hFunctions import *


# disable proxy usage
proxy_handler = urllib2.ProxyHandler({})
opener = urllib2.build_opener(proxy_handler)
urllib2.install_opener(opener)

req = urllib2.Request('http://etl-hdp-mgmt.bi.b2e.prod.maxus.lan/elastic/logstash*/_search') #prod
reqBody = open('jsonRequest.txt').read()
req.data = reqBody
response = urllib2.urlopen(req)
output = response.read()
data = json.loads(output)

for job in data.get("hits"):
    esOut = data.get("hits").get("hits")

scriptDir = os.path.dirname(os.path.abspath(__file__))
conn = sqlite3.connect(os.path.join(scriptDir, 'jobDuration.db'))
conn.text_factory = str

c = conn.cursor()
c.execute('''DROP TABLE IF EXISTS jobDuration''')
c.execute('''DROP TABLE IF EXISTS stdDeviation''')
conn.commit()

c.execute('''CREATE TABLE jobDuration (WFname TEXT, Finish DATE, Duration_in_sec INTEGER, yarnAppID TEXT)''')
c.execute('''CREATE TABLE stdDeviation (WFname TEXT, meanDuration INTEGER, stdDeviation REAL)''')
conn.commit()

nError = 0
for job in esOut:
    source = job.get("_source")
    coordName = source.get("coord_name")
    timeFinish = source.get("@timestamp")
    duration = source.get("dur")
    yarnAppID = source.get("H_app_id")
    oozieID =  source.get("oozie_id")
    if coordName == None or yarnAppID == None or oozieID == None:
        nError += 1
    if coordName != None:
        insertIntoJobDuration(c, coordName, timeFinish, duration, yarnAppID)
conn.commit()
print "bad data: " + str(nError)

selectjobDuration = 'SELECT * FROM jobDuration'
selectDistinctJobs = 'SELECT WFname FROM stdDeviation'
c.execute(selectjobDuration)
result = c.fetchall()
err = 0
for job in result:
    try:
        wfname = job[0].decode('utf-8', errors='replace')
        #print translit(wfname, 'ru', reversed=True)
    except AttributeError:
        print job
        err = err + 1
        pass
print "Errors: " + str(err)

c.execute('''INSERT INTO stdDeviation ( WFname )
            SELECT  DISTINCT WFname
            FROM    jobDuration''')
conn.commit()

c.execute(selectDistinctJobs)
result = c.fetchall()
result = sql_result_to_list(result)
for job in result:
    c.execute("SELECT Duration_in_sec FROM jobDuration where WFname = ?", (job,))
    jobDurations = c.fetchall()
    jobDurations = sql_result_to_list(jobDurations)
    meanDuration = sum(jobDurations) * 1.0 / len(jobDurations)
    k = 0
    for d in jobDurations:
        delta = d * 1.0 - meanDuration
        k += delta*delta
    stdDeviation = math.sqrt(k / len(jobDurations))
    print job.decode('utf-8', errors='replace'), meanDuration, median(jobDurations), stdDeviation
    c.execute("""UPDATE stdDeviation
                 SET meanDuration = ?,
                     stdDeviation = ?
                  where WFname = ?""", (meanDuration, stdDeviation, job,))
conn.commit()
conn.close()
