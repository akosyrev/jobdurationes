__author__ = 'akosyrev'


def median(lst):
    lst = sorted(lst)
    if len(lst) < 1:
            return None
    if len(lst) %2 == 1:
            return lst[((len(lst)+1)/2)-1]
    else:
            return float(sum(lst[(len(lst)/2)-1:(len(lst)/2)+1]))/2.0


def sql_result_to_list(res):
    lst = []
    for i in res:
        lst.append(i[0])
    return lst


### SQLite
def insertIntoJobDuration(dbconn, wfname, finish, duration, yarnAppID ):
    dbconn.execute('INSERT INTO jobDuration (WFname, Finish, Duration_in_sec, yarnAppID) VALUES (?, ?, ?, ?)',  [wfname,
                                                                                                   finish,
                                                                                                   duration,
                                                                                                   yarnAppID])
